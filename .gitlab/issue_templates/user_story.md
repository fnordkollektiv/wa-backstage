## Story
(Try to think as a user/customer... not a developer. Keep it fine grained and precise(no epic).)
As a [type of user],
I want [to perform some task],
so that I can [achieve some goal].

## Acceptance Criteria
(Is it complex business logic, permissions, etc...? Then add some Scenarios here...)
Scenario A: [What should happen]
Given that [some context],
when [some action is done],
then [such outcomes are expected to occur].

## Notes
(Useful extra information)

## References
(Is this related or dependand to other US? Is there a document or a Wiki article...?)
