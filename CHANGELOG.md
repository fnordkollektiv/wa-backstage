# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org).

## [Unreleased]


## [1.0] 2022-06-10
### Added
- Authentication
- Responsive UI
- Slick design
- Backstage user account management (i.e. management of accounts who can administrate a WA instance)
- Player management
- Character texture management
- Room management
- World management
- Tag management
- Communication with WA-API
