# Contributing guidelines

This document describes the way we want to structure our collective work on the workadventure backend implementation.


## Git branching model & merge requests

* For each gitlab issue we create a new branch (issue number referenced in branch name)
    - Suggested naming convention: ```[feature | bugfix]/<ISSUE NUMBER>-<ACTION TITLE>```
    - exceptions are possible: for minor bugfixes / hotfixes, the commit can be directly applied to the _develop_ branch (see _Minor bugfixes section_ below)
* If you are done with your work on an issue, the related issue branch will be reviewed
* Once the review is successfully done and the issue branch is approved by another human (& automated tests are passing), then the issue branch is merged into the _develop_ branch
    - After the merge, it might make sense to add the features you just implemented to the _Unreleased_ section of the ```CHANGELOG.md``` file
* The _develop_ branch should always stay stable and possible to build
* Later we will introduce a _main_ branch, into which we merge the develop branch and tag version commits for easy consumption and use from an external, outside perspective


## Commits

### Automated linting: Git pre-commit setup

We want to setup a pre-commit git hook which preprocesses our changes, so that we always stick to our style guides and linting configuration and therefore only push _high code quality_ commits.

In order to setup the `pre-commit` git hook, run the following command:

``` bash
pre-commit install
```


### Conventional commit messages

We agreed on trying to use [conventional commit messages](https://www.conventionalcommits.org/en/v1.0.0/).

Make sure to reference the related issue in the first line of your commit message, to easily map commits to issues.

### Minor bugfixes

* Hotfixes on _develop_ branch for smaller bugs are fine (you can do it if you feel able to fix it on the fly)
    - Tell the person who made the mistake via a PM, if you think it is not something important for the team or a reappearing mistake among the team.
    - On the other hand, If you think there is a certain learning for the whole team, then post an explanation to our communication channel without blaming the person who made the mistake


## Versioning

We will use [semantic versioning](https://semver.org/) and bump versions accordingly.

>Given a version number MAJOR.MINOR.PATCH, increment the:
>
>   1. MAJOR version when you make incompatible API changes,
>   2. MINOR version when you add functionality in a backwards compatible manner, and
>   3. PATCH version when you make backwards compatible bug fixes.
>
>Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.


## Release preparation

* Write a fitting commit message containing all important changes (i.e. changelog) and push stable version commits to **main branch**
* Use git tags to label stable version commits, makes it easy to consume individual versions for automated building pipelines and in general from an outside perspective (e.g. other projects depending on our codebase)
* Adapt ```CHANGELOG.md``` according to [keepachangelog.com](https://keepachangelog.com/en/1.0.0/)


## Documentation
We have decided to use [handsdown](https://vemel.github.io/handsdown/) for automatic documentation generation with [PEP 257](https://www.python.org/dev/peps/pep-0257/) as formatting convention (for docstrings).

To manually generate markdown documentation files in `docs` run
```handsdown```
