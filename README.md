# WA-Backstage

Admin backend for Workadventure

## Requirements

Tested with Python 3.9.

Node.js 14.0

npm  6

## Quick Development Setup

Bootstrap your venv and project (you’ll need to do this only once):

```shell
# Create a virtual environment
python3 -m venv venv
# Activate your venv
. venv/bin/activate
# Install dependencies
pip install -r requirements_dev.txt
# Install node dependencies
python manage.py tailwind install
```

In the future just run:
```shell
# Activate your venv
. venv/bin/activate
# Configure the settings
export DJANGO_SETTINGS_MODULE=wa_backstage.settings
# Apply database migrations
python3 manage.py migrate
```

To populate the database with some test data, run:

```shell
python3 manage.py init-seed
```
During development you will most likely find, that these are not sufficient. You can (and are very welcome to) add more test data to 

``` shell
core/management/commands/init-seed.py
```

To clear the database from test data run

``` shell
python3 manage.py flush
```

Start the development server with:
```shell
python3 manage.py runserver
```

And to run the tests:
```shell
tox
```

For just-in-time compilation of css classes:
```shell
python manage.py tailwind start
```

## Production setup

Please see [INSTALLATION.md](./INSTALLATION.md).
