from django.core.management.base import BaseCommand

from core.models import AdminTag, Ban, CharacterTexture, MembershipWorld, Player, Report, Room, Tag, World


class TestData:
    """
    This class contains the lists of test data
    """

    def __init__(self):
        self.worlds = [
            World(
                name="Test World1",
                description="Its such a crazy world, do you dare to check it out?",
                url="https://youtube.com",
            ),
            World(name="Moon", description="Let`s check out the dark side of the moon", url="https://wikipedia.org"),
        ]

        self.tags = [
            Tag(name="Cat", description="Everybody wants to be cat."),
            Tag(name="Dog", description="Dogs are nice too."),
            Tag(name="Tag1", description="first Tag ever"),
            AdminTag(),
        ]
        self.players = [
            Player(
                username="funkypunky", email="funky@punky.wtf", comment="They asked if they could bring their dogs?!"
            ),
            Player(
                username="queerglitter",
                email="glitteryrainbow@club.org",
                comment="Glitter, trash pop music and champagne for everyone",
            ),
            Player(username="emma", email="emma@goldman.org", comment="No revolution without dancing."),
            Player(username="alex", email="alex@berkman.org", comment="Emma is awesome."),
            Player(username="thorsten", email="thorstenschneider@polizei-hessen.de", comment="Ban Test Dummy"),
            Player(username="melanie", email="melanieschmidt@polizei-hessen.de", comment="Ban Test Dummy"),
            Player(username="ismail", email="ismailaslan@polizei-hessen.de", comment="Ban Test Dummy"),
        ]
        self.rooms = [
            Room(
                name="Hallway",
                description="All creatures welcome",
                is_entry_room=True,
                world=self.worlds[0],
                url="/_/unseretollewelt/unsertollerraum",
                map_url="https://git.fnordkollektiv.de/pub/fnordworld/-/raw/main/map.json",
                policy_type=2,
            )
        ]
        self.membershipsworld = [
            MembershipWorld(player=self.players[0], world=self.worlds[0]),
            MembershipWorld(player=self.players[1], world=self.worlds[0]),
            MembershipWorld(player=self.players[0], world=self.worlds[1]),
            MembershipWorld(player=self.players[1], world=self.worlds[1]),
            MembershipWorld(player=self.players[2], world=self.worlds[1]),
            MembershipWorld(player=self.players[3], world=self.worlds[1]),
            MembershipWorld(player=self.players[4], world=self.worlds[1]),
            MembershipWorld(player=self.players[5], world=self.worlds[1]),
            MembershipWorld(player=self.players[6], world=self.worlds[1]),
        ]
        self.reports = [
            Report(
                room=self.rooms[0],
                reported_user=self.players[0],
                reporter_user=self.players[1],
                user_comment="They are evil!",
            ),
        ]
        self.bans = [
            Ban(
                membership_world=self.membershipsworld[1],
                room=self.rooms[0],
                message="You are evil!",
            ),
            Ban(membership_world=self.membershipsworld[6], room=None, message="No Cops please"),
            Ban(membership_world=self.membershipsworld[7], room=None, message="No Cops please"),
            Ban(membership_world=self.membershipsworld[8], room=None, message="No Cops please"),
        ]
        self.character_textures = [
            CharacterTexture(
                name="Texture1",
                url="https://gitlab.com/wa-admin-panel/wa-admin-panel-backend/\
                uploads/a368f493d298b4b0b30b085100970a26/Headmaster_fmale.png",
                texture_id=1,
                level=1,
                policy_type=1,
                is_deactivated=False,
            ),
        ]

        self.obj_m2ms = []
        self.obj_m2ms.append({"obj": self.character_textures[0], "field": "tags", "relations": [self.tags[0]]})
        self.obj_m2ms.append({"obj": self.character_textures[0], "field": "worlds", "relations": [self.worlds[0]]})

    def aggregate_objects(self):
        """
        The aggregate_objects function is used to export an overall list of all
        test objects which are to be written to the database
        """
        # Add more lists here
        return (
            self.worlds
            + self.tags
            + self.players
            + self.rooms
            + self.membershipsworld
            + self.reports
            + self.bans
            + self.character_textures
        )


class Command(BaseCommand):
    """This django manage.py command 'init-seed' seeds some data to the database
    for testing purposes

    If you want to delete the seeded test data, run:
    python manage.py flush"""

    help = "Initial database seeding"

    # def add_arguments(self, parser):
    # Named (optional) arguments
    # parser.add_argument(
    #     "--delete",
    #     action="store_true",
    #     help="Delete the seeded information from the database",
    # )

    def handle(self, *args, **options):
        test_data = TestData()
        for obj in test_data.aggregate_objects():
            obj.save()
        for entry in test_data.obj_m2ms:
            getattr(entry["obj"], entry["field"]).set(entry["relations"])
            # sys.stdout.write("Object with ID: %d saved \n" % obj.id)
