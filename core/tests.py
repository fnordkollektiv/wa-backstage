from django.core.management import call_command
from django.test import Client
from django.test import TestCase  # noqa: F401
from django.urls import reverse

from .models import BackstageUser as User, CharacterTexture, MembershipWorld
from .models import Player, Room, Tag, World


class ManagementCommandsTest(TestCase):
    """Tests for custom management commands."""

    def test_init_seed_adds_players(self):
        "Test whether manage.py init-seed command runs and adds players to database."
        self.assertTrue(not Player.objects.all())
        call_command("init-seed")
        self.assertFalse(not Player.objects.all())

    def test_init_seed_adds_tags(self):
        "Test whether manage.py init-seed command runs and adds tags to database."
        self.assertTrue(not Tag.objects.all())
        call_command("init-seed")
        self.assertFalse(not Tag.objects.all())

    def test_init_seed_adds_worlds(self):
        "Test whether manage.py init-seed command runs and adds worlds to database."
        self.assertTrue(not World.objects.all())
        call_command("init-seed")
        self.assertFalse(not World.objects.all())

    def test_init_seed_adds_rooms(self):
        "Test whether manage.py init-seed command runs and adds rooms to database."
        self.assertTrue(not Room.objects.all())
        call_command("init-seed")
        self.assertFalse(not Room.objects.all())


class AuthTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username="Emma", email=None, password="Goldman")

    def test_trying_to_access_without_login_redirects_to_login(self):

        # This is currently testing only the first layer of the tree, probably needs to be extended to all of them?
        # Also, to keep this tes transparent, readable and provide fine grained control
        # (e.g. exclude the URLs that should be accessible without login),
        # each url is tested in an explicit block of assertions.
        # If the complexity increases further, then it might become necessary to use a loop of subtests.
        response = self.client.get("/")

        self.assertEqual(response.status_code, 302)  # Assert, that a redirect happens
        self.assertRegex(
            response.url, r"/accounts/login/*"
        )  # Assert, that the view to which user has been redirected is the login view.

        response = self.client.get("/unicorn/")
        self.assertEqual(response.status_code, 302)  # Assert, that a redirect happens
        self.assertRegex(
            response.url, r"/accounts/login/*"
        )  # Assert, that the view to which user has been redirected is the login view.

        # response = self.client.get("/wa-api/")
        # self.assertEqual(response.status_code, 302)  # Assert, that a redirect happens
        # self.assertRegex(response.url, r"/accounts/login/*")

        response = self.client.get("/accounts/")
        self.assertEqual(response.status_code, 302)  # Assert, that a redirect happens
        self.assertRegex(response.url, r"/accounts/login/*")

    def test_login_allows_access_to_url(self):
        url = reverse("about")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class LogicTest(TestCase):
    def setUp(self):
        call_command("init-seed")

    def test_world_slug(self):
        world = World(name="Wörld afsdfpo")
        world.save()
        self.assertEqual(world.slug, "world-afsdfpo")

    def test_character_texture_assignment(self):
        # policy_type 1
        ct = CharacterTexture(texture_id=99, level=3, policy_type=1, url="https://some.url")
        ct.save()
        # not associated with a world? not used
        self.assertFalse(
            MembershipWorld.objects.filter(character_textures__in=[ct]), msg="not associated with a world? not used"
        )
        # associated with a world? should be with all members incl. anon
        world = World.objects.first()
        ct.world_set.add(world)
        self.assertTrue(MembershipWorld.objects.filter(character_textures__in=[ct], world=world, anonymous=True))
        self.assertEqual(
            MembershipWorld.objects.filter(character_textures__in=[ct], world=world).count(),
            MembershipWorld.objects.filter(world=world).count(),
            msg="associated with a world? should be with all members incl. anon",
        )
        # deleting the association with the world should undo
        ct.world_set.remove(world)
        self.assertFalse(
            MembershipWorld.objects.filter(character_textures__in=[ct], world=world),
            msg="deleting the association with the world should undo",
        )
        # policy_type 2
        ct.world_set.add(world)
        ct.policy_type = 2
        ct.save()
        # now anonymous member should have lost it
        self.assertFalse(MembershipWorld.objects.filter(character_textures__in=[ct], world=world, anonymous=True))
        self.assertEqual(
            MembershipWorld.objects.filter(character_textures__in=[ct], world=world).count(),
            MembershipWorld.objects.filter(world=world).count() - 1,
            msg="now anonymous member should have lost it",
        )
        # policy_type 3
        ct.policy_type = 3
        ct.save()
        # now we should have tags not assigned anywhere anymore
        self.assertFalse(
            MembershipWorld.objects.filter(character_textures__in=[ct], world=world),
            msg="now we should have tags not assigned anywhere anymore",
        )
        # with a tag defined still not
        tag = Tag(name="taggy tag")
        tag.save()
        ct.tags.add(tag)
        self.assertFalse(
            MembershipWorld.objects.filter(character_textures__in=[ct], world=world),
            msg="with a tag defined on one side still not",
        )
        ct2 = CharacterTexture(texture_id=98, level=3, policy_type=1, url="https://some.url")
        ct2.save()
        ct2.world_set.add(world)
        # when a member gets this tag they should get the textures assigned automaticaly + CTs w/o tags restriction
        member = MembershipWorld.objects.filter(world=world, anonymous=False).first()
        member.tags.add(tag)
        self.assertEqual(
            MembershipWorld.objects.filter(character_textures__in=[ct], world=world).first(),
            member,
            msg="when a member gets this tag they should get the textures assigned automaticaly",
        )
        self.assertEqual(
            member.character_textures.all().count(),
            2,
            msg="member should have both CTs with both policies assigned now",
        )
        # the CT gets removed again, when the tag is removed from the CT
        ct.tags.remove(tag)
        self.assertFalse(MembershipWorld.objects.filter(character_textures__in=[ct], world=world))
        self.assertEqual(
            member.character_textures.all().count(),
            1,
            msg="member should have only one CT left now",
        )
