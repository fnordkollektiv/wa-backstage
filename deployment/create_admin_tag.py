from django.core.management.base import BaseCommand

from core.models import AdminTag


class Command(BaseCommand):
    help = "Create the special admin tag"

    def handle(self, *args, **options):
        tag = AdminTag()
        try:
            tag.save()
        except TypeError:
            pass
