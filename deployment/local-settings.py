"""Local settings
"""

from .common import *  # noqa: F401, F403


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "",
        "USER": "",
        "PASSWORD": "",
        "HOST": "db",
        "PORT": "",
    }
}

# Fully-qualified domain name of the instance
ALLOWED_HOSTS = [""]

# A secret key for cryptographic operations (=> e.g. session cookies)
SECRET_KEY = ""

# API token for Workadventure instances connecting to this backstage instance
ADMIN_API_TOKEN = ""

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
EMAIL_HOST = ""
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = ""
EMAIL_HOST_PASSWORD = ""
DEFAULT_FROM_EMAIL = ""
