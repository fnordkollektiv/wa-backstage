# Apps

> Auto-generated documentation for [core.apps](blob/master/core/apps.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Core](index.md#core) / Apps
    - [CoreConfig](#coreconfig)
        - [CoreConfig().ready](#coreconfigready)

## CoreConfig

[[find in source code]](blob/master/core/apps.py#L4)

```python
class CoreConfig(AppConfig):
```

### CoreConfig().ready

[[find in source code]](blob/master/core/apps.py#L8)

```python
def ready():
```
