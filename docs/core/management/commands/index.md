# Commands

> Auto-generated documentation for [core.management.commands](blob/master/core/management/commands/__init__.py) module.

- [Backstage](../../../README.md#wa-backstage) / [Modules](../../../MODULES.md#backstage-modules) / [Core](../../index.md#core) / [Management](../index.md#management) / Commands
    - Modules
        - [Init-seed](init-seed.md#init-seed)
