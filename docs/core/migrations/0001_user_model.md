# 0001 User Model

> Auto-generated documentation for [core.migrations.0001_user_model](blob/master/core/migrations/0001_user_model.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / [Migrations](index.md#migrations) / 0001 User Model
    - [Migration](#migration)

## Migration

[[find in source code]](blob/master/core/migrations/0001_user_model.py#L9)

```python
class Migration(migrations.Migration):
```
