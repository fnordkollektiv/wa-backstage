# 0002 Initial Models

> Auto-generated documentation for [core.migrations.0002_initial_models](blob/master/core/migrations/0002_initial_models.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / [Migrations](index.md#migrations) / 0002 Initial Models
    - [Migration](#migration)

## Migration

[[find in source code]](blob/master/core/migrations/0002_initial_models.py#L8)

```python
class Migration(migrations.Migration):
```
