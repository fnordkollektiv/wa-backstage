# 0004 Alter Membershipworld Unique Together

> Auto-generated documentation for [core.migrations.0004_alter_membershipworld_unique_together](blob/master/core/migrations/0004_alter_membershipworld_unique_together.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / [Migrations](index.md#migrations) / 0004 Alter Membershipworld Unique Together
    - [Migration](#migration)

## Migration

[[find in source code]](blob/master/core/migrations/0004_alter_membershipworld_unique_together.py#L6)

```python
class Migration(migrations.Migration):
```
