# 0009 Alter Room Unique Together

> Auto-generated documentation for [core.migrations.0009_alter_room_unique_together](blob/master/core/migrations/0009_alter_room_unique_together.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / [Migrations](index.md#migrations) / 0009 Alter Room Unique Together
    - [Migration](#migration)

## Migration

[[find in source code]](blob/master/core/migrations/0009_alter_room_unique_together.py#L6)

```python
class Migration(migrations.Migration):
```
