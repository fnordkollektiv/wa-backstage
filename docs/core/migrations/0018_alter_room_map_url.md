# 0018 Alter Room Map Url

> Auto-generated documentation for [core.migrations.0018_alter_room_map_url](blob/master/core/migrations/0018_alter_room_map_url.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / [Migrations](index.md#migrations) / 0018 Alter Room Map Url
    - [Migration](#migration)

## Migration

[[find in source code]](blob/master/core/migrations/0018_alter_room_map_url.py#L7)

```python
class Migration(migrations.Migration):
```
