# Tests

> Auto-generated documentation for [core.tests](blob/master/core/tests.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Core](index.md#core) / Tests
    - [AuthTest](#authtest)
        - [AuthTest().setUp](#authtestsetup)
        - [AuthTest().test_login_allows_access_to_url](#authtesttest_login_allows_access_to_url)
        - [AuthTest().test_trying_to_access_without_login_redirects_to_login](#authtesttest_trying_to_access_without_login_redirects_to_login)
    - [LogicTest](#logictest)
        - [LogicTest().setUp](#logictestsetup)
        - [LogicTest().test_character_texture_assignment](#logictesttest_character_texture_assignment)
        - [LogicTest().test_world_slug](#logictesttest_world_slug)
    - [ManagementCommandsTest](#managementcommandstest)
        - [ManagementCommandsTest().test_init_seed_adds_players](#managementcommandstesttest_init_seed_adds_players)
        - [ManagementCommandsTest().test_init_seed_adds_rooms](#managementcommandstesttest_init_seed_adds_rooms)
        - [ManagementCommandsTest().test_init_seed_adds_tags](#managementcommandstesttest_init_seed_adds_tags)
        - [ManagementCommandsTest().test_init_seed_adds_worlds](#managementcommandstesttest_init_seed_adds_worlds)

## AuthTest

[[find in source code]](blob/master/core/tests.py#L38)

```python
class AuthTest(TestCase):
```

### AuthTest().setUp

[[find in source code]](blob/master/core/tests.py#L39)

```python
def setUp():
```

### AuthTest().test_login_allows_access_to_url

[[find in source code]](blob/master/core/tests.py#L71)

```python
def test_login_allows_access_to_url():
```

### AuthTest().test_trying_to_access_without_login_redirects_to_login

[[find in source code]](blob/master/core/tests.py#L43)

```python
def test_trying_to_access_without_login_redirects_to_login():
```

## LogicTest

[[find in source code]](blob/master/core/tests.py#L80)

```python
class LogicTest(TestCase):
```

### LogicTest().setUp

[[find in source code]](blob/master/core/tests.py#L81)

```python
def setUp():
```

### LogicTest().test_character_texture_assignment

[[find in source code]](blob/master/core/tests.py#L89)

```python
def test_character_texture_assignment():
```

### LogicTest().test_world_slug

[[find in source code]](blob/master/core/tests.py#L84)

```python
def test_world_slug():
```

## ManagementCommandsTest

[[find in source code]](blob/master/core/tests.py#L10)

```python
class ManagementCommandsTest(TestCase):
```

Tests for custom management commands.

### ManagementCommandsTest().test_init_seed_adds_players

[[find in source code]](blob/master/core/tests.py#L13)

```python
def test_init_seed_adds_players():
```

Test whether manage.py init-seed command runs and adds players to database.

### ManagementCommandsTest().test_init_seed_adds_rooms

[[find in source code]](blob/master/core/tests.py#L31)

```python
def test_init_seed_adds_rooms():
```

Test whether manage.py init-seed command runs and adds rooms to database.

### ManagementCommandsTest().test_init_seed_adds_tags

[[find in source code]](blob/master/core/tests.py#L19)

```python
def test_init_seed_adds_tags():
```

Test whether manage.py init-seed command runs and adds tags to database.

### ManagementCommandsTest().test_init_seed_adds_worlds

[[find in source code]](blob/master/core/tests.py#L25)

```python
def test_init_seed_adds_worlds():
```

Test whether manage.py init-seed command runs and adds worlds to database.
