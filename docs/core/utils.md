# Utils

> Auto-generated documentation for [core.utils](blob/master/core/utils.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Core](index.md#core) / Utils
    - [SingletonModel](#singletonmodel)
        - [SingletonModel.load](#singletonmodelload)
        - [SingletonModel().save](#singletonmodelsave)
    - [WritableSerializerMethodField](#writableserializermethodfield)
        - [WritableSerializerMethodField().bind](#writableserializermethodfieldbind)
        - [WritableSerializerMethodField().get_default](#writableserializermethodfieldget_default)
        - [WritableSerializerMethodField().to_internal_value](#writableserializermethodfieldto_internal_value)

## SingletonModel

[[find in source code]](blob/master/core/utils.py#L5)

```python
class SingletonModel(models.Model):
```

Singleton Django Model

### SingletonModel.load

[[find in source code]](blob/master/core/utils.py#L19)

```python
@classmethod
def load():
```

Load object from the database. Failing that, create a new empty
(default) instance of the object and return it (without saving it
to the database).

### SingletonModel().save

[[find in source code]](blob/master/core/utils.py#L11)

```python
def save(*args, **kwargs):
```

Save object to the database. Removes all other entries if there
are any.

## WritableSerializerMethodField

[[find in source code]](blob/master/core/utils.py#L33)

```python
class WritableSerializerMethodField(serializers.SerializerMethodField):
    def __init__(**kwargs):
```

A field for DRF similar to build in SerializerMethodField, but writeable

### WritableSerializerMethodField().bind

[[find in source code]](blob/master/core/utils.py#L46)

```python
def bind(field_name, parent):
```

### WritableSerializerMethodField().get_default

[[find in source code]](blob/master/core/utils.py#L53)

```python
def get_default():
```

### WritableSerializerMethodField().to_internal_value

[[find in source code]](blob/master/core/utils.py#L58)

```python
def to_internal_value(data):
```
