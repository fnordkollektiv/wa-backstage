# Apps

> Auto-generated documentation for [frontend.apps](blob/master/frontend/apps.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Frontend](index.md#frontend) / Apps
    - [FrontendConfig](#frontendconfig)

## FrontendConfig

[[find in source code]](blob/master/frontend/apps.py#L4)

```python
class FrontendConfig(AppConfig):
```
