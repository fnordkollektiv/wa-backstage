# Backstage Users

> Auto-generated documentation for [frontend.components.backstage_users](blob/master/frontend/components/backstage_users.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Frontend](../index.md#frontend) / [Components](index.md#components) / Backstage Users
    - [BackstageUsersView](#backstageusersview)
        - [BackstageUsersView().get_form_data](#backstageusersviewget_form_data)
        - [BackstageUsersView().set_attributes](#backstageusersviewset_attributes)

## BackstageUsersView

[[find in source code]](blob/master/frontend/components/backstage_users.py#L9)

```python
class BackstageUsersView(
    CreateModeMixin,
    DeleteModeMixin,
    UpdateModeMixin,
    GenericModeView,
):
```

#### See also

- [CreateModeMixin](../../unicorn_viewsets/mixins.md#createmodemixin)
- [DeleteModeMixin](../../unicorn_viewsets/mixins.md#deletemodemixin)
- [GenericModeView](../../unicorn_viewsets/views.md#genericmodeview)
- [UpdateModeMixin](../../unicorn_viewsets/mixins.md#updatemodemixin)

### BackstageUsersView().get_form_data

[[find in source code]](blob/master/frontend/components/backstage_users.py#L17)

```python
def get_form_data():
```

### BackstageUsersView().set_attributes

[[find in source code]](blob/master/frontend/components/backstage_users.py#L23)

```python
def set_attributes():
```
