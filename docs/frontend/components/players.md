# Players

> Auto-generated documentation for [frontend.components.players](blob/master/frontend/components/players.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Frontend](../index.md#frontend) / [Components](index.md#components) / Players
    - [PlayersView](#playersview)
        - [PlayersView().get_form_data](#playersviewget_form_data)
        - [PlayersView().set_attributes](#playersviewset_attributes)

## PlayersView

[[find in source code]](blob/master/frontend/components/players.py#L10)

```python
class PlayersView(
    ReferralMixin,
    CreateModeMixin,
    DeleteModeMixin,
    UpdateModeMixin,
    GenericModeView,
):
```

#### See also

- [CreateModeMixin](../../unicorn_viewsets/mixins.md#createmodemixin)
- [DeleteModeMixin](../../unicorn_viewsets/mixins.md#deletemodemixin)
- [GenericModeView](../../unicorn_viewsets/views.md#genericmodeview)
- [ReferralMixin](../views.md#referralmixin)
- [UpdateModeMixin](../../unicorn_viewsets/mixins.md#updatemodemixin)

### PlayersView().get_form_data

[[find in source code]](blob/master/frontend/components/players.py#L19)

```python
def get_form_data():
```

### PlayersView().set_attributes

[[find in source code]](blob/master/frontend/components/players.py#L26)

```python
def set_attributes():
```
