# Tags

> Auto-generated documentation for [frontend.components.tags](blob/master/frontend/components/tags.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Frontend](../index.md#frontend) / [Components](index.md#components) / Tags
    - [TagsView](#tagsview)
        - [TagsView().get_form_data](#tagsviewget_form_data)
        - [TagsView().set_attributes](#tagsviewset_attributes)

## TagsView

[[find in source code]](blob/master/frontend/components/tags.py#L9)

```python
class TagsView(
    CreateModeMixin,
    DeleteModeMixin,
    UpdateModeMixin,
    GenericModeView,
):
```

#### See also

- [CreateModeMixin](../../unicorn_viewsets/mixins.md#createmodemixin)
- [DeleteModeMixin](../../unicorn_viewsets/mixins.md#deletemodemixin)
- [GenericModeView](../../unicorn_viewsets/views.md#genericmodeview)
- [UpdateModeMixin](../../unicorn_viewsets/mixins.md#updatemodemixin)

### TagsView().get_form_data

[[find in source code]](blob/master/frontend/components/tags.py#L17)

```python
def get_form_data():
```

### TagsView().set_attributes

[[find in source code]](blob/master/frontend/components/tags.py#L23)

```python
def set_attributes():
```
