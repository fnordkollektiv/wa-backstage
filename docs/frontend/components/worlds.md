# Worlds

> Auto-generated documentation for [frontend.components.worlds](blob/master/frontend/components/worlds.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Frontend](../index.md#frontend) / [Components](index.md#components) / Worlds
    - [WorldsView](#worldsview)
        - [WorldsView().get_form_data](#worldsviewget_form_data)
        - [WorldsView().set_attributes](#worldsviewset_attributes)

## WorldsView

[[find in source code]](blob/master/frontend/components/worlds.py#L9)

```python
class WorldsView(
    CreateModeMixin,
    UpdateModeMixin,
    DeleteModeMixin,
    GenericModeView,
):
```

#### See also

- [CreateModeMixin](../../unicorn_viewsets/mixins.md#createmodemixin)
- [DeleteModeMixin](../../unicorn_viewsets/mixins.md#deletemodemixin)
- [GenericModeView](../../unicorn_viewsets/views.md#genericmodeview)
- [UpdateModeMixin](../../unicorn_viewsets/mixins.md#updatemodemixin)

### WorldsView().get_form_data

[[find in source code]](blob/master/frontend/components/worlds.py#L24)

```python
def get_form_data():
```

### WorldsView().set_attributes

[[find in source code]](blob/master/frontend/components/worlds.py#L18)

```python
def set_attributes():
```
