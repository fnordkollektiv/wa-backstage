# Frontend

> Auto-generated documentation for [frontend](blob/master/frontend/__init__.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / Frontend
    - Modules
        - [Admin](admin.md#admin)
        - [Apps](apps.md#apps)
        - [Components](components/index.md#components)
        - [Forms](forms.md#forms)
        - [Templatetags](templatetags/index.md#templatetags)
        - [Tests](tests.md#tests)
        - [Urls](urls.md#urls)
        - [Utils](utils.md#utils)
        - [Views](views.md#views)
