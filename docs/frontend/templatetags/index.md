# Templatetags

> Auto-generated documentation for [frontend.templatetags](blob/master/frontend/templatetags/__init__.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Frontend](../index.md#frontend) / Templatetags
    - Modules
        - [Frontend](frontend.md#frontend)
