# Utils

> Auto-generated documentation for [frontend.utils](blob/master/frontend/utils.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Frontend](index.md#frontend) / Utils
    - [FilterSelectWidgetDecorator](#filterselectwidgetdecorator)
        - [FilterSelectWidgetDecorator.main__get_form_data](#filterselectwidgetdecoratormain__get_form_data)
        - [FilterSelectWidgetDecorator.main__set_attributes](#filterselectwidgetdecoratormain__set_attributes)

## FilterSelectWidgetDecorator

[[find in source code]](blob/master/frontend/utils.py#L4)

```python
class FilterSelectWidgetDecorator():
    def __init__(*decorator_args, **decorator_kwargs):
```

This is a decorator to add autocomplete widgets to unicorn components
use as a class decorator to the component:
@FilterSelectWidgetDecorator(queryset=<qs>, form_field="field_name")
Managed items must have pk and name fields

### FilterSelectWidgetDecorator.main__get_form_data

[[find in source code]](blob/master/frontend/utils.py#L17)

```python
@staticmethod
def main__get_form_data(self):
```

### FilterSelectWidgetDecorator.main__set_attributes

[[find in source code]](blob/master/frontend/utils.py#L11)

```python
@staticmethod
def main__set_attributes(self):
```
