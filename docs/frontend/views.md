# Views

> Auto-generated documentation for [frontend.views](blob/master/frontend/views.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Frontend](index.md#frontend) / Views
    - [AboutView](#aboutview)
        - [AboutView().get_context_data](#aboutviewget_context_data)
    - [CSVResponse](#csvresponse)
    - [ComponentView](#componentview)
    - [PlayerExportView](#playerexportview)
        - [PlayerExportView().get](#playerexportviewget)
    - [PlayerImportView](#playerimportview)
        - [PlayerImportView().form_valid](#playerimportviewform_valid)
    - [ReferralMixin](#referralmixin)
        - [ReferralMixin().get_success_url](#referralmixinget_success_url)
        - [ReferralMixin().needs_page_reload](#referralmixinneeds_page_reload)
    - [WorldComponentView](#worldcomponentview)
        - [WorldComponentView().get](#worldcomponentviewget)

## AboutView

[[find in source code]](blob/master/frontend/views.py#L31)

```python
class AboutView(TemplateView):
```

### AboutView().get_context_data

[[find in source code]](blob/master/frontend/views.py#L34)

```python
def get_context_data(**kwargs):
```

## CSVResponse

[[find in source code]](blob/master/frontend/views.py#L55)

```python
class CSVResponse(HttpResponse):
    def __init__(content, filename, *args, **kwargs):
```

## ComponentView

[[find in source code]](blob/master/frontend/views.py#L39)

```python
class ComponentView(views.ComponentView):
```

## PlayerExportView

[[find in source code]](blob/master/frontend/views.py#L61)

```python
class PlayerExportView(View):
```

### PlayerExportView().get

[[find in source code]](blob/master/frontend/views.py#L62)

```python
def get(request, *args, **kwargs):
```

## PlayerImportView

[[find in source code]](blob/master/frontend/views.py#L68)

```python
class PlayerImportView(FormView):
```

### PlayerImportView().form_valid

[[find in source code]](blob/master/frontend/views.py#L73)

```python
def form_valid(form):
```

## ReferralMixin

[[find in source code]](blob/master/frontend/views.py#L17)

```python
class ReferralMixin():
    def __init__(**kwargs):
```

### ReferralMixin().get_success_url

[[find in source code]](blob/master/frontend/views.py#L27)

```python
def get_success_url():
```

### ReferralMixin().needs_page_reload

[[find in source code]](blob/master/frontend/views.py#L24)

```python
def needs_page_reload():
```

## WorldComponentView

[[find in source code]](blob/master/frontend/views.py#L43)

```python
class WorldComponentView(SingleObjectMixin, views.ComponentView):
```

### WorldComponentView().get

[[find in source code]](blob/master/frontend/views.py#L49)

```python
def get(request, *args, **kwargs):
```
