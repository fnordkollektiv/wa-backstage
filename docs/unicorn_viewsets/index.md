# Unicorn Viewsets

> Auto-generated documentation for [unicorn_viewsets](blob/master/unicorn_viewsets/__init__.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / Unicorn Viewsets
    - Modules
        - [Mixins](mixins.md#mixins)
        - [Routers](routers.md#routers)
        - [Views](views.md#views)
