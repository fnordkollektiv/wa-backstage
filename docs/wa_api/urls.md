# Urls

> Auto-generated documentation for [wa_api.urls](blob/master/wa_api/urls.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Wa Api](index.md#wa-api) / Urls

#### Attributes

- `router` - define DRF routes: `routers.DefaultRouter()`
- `urlpatterns` - define django routes: `[path('', include(router.urls)), path('api-auth...`
