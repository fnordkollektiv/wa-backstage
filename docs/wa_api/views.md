# Views

> Auto-generated documentation for [wa_api.views](blob/master/wa_api/views.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Wa Api](index.md#wa-api) / Views
    - [BanFilter](#banfilter)
    - [CheckBanView](#checkbanview)
        - [CheckBanView().get](#checkbanviewget)
        - [CheckBanView().get_object](#checkbanviewget_object)
    - [CrudObjectAccessViolationMixin](#crudobjectaccessviolationmixin)
        - [CrudObjectAccessViolationMixin().get](#crudobjectaccessviolationmixinget)
    - [LoginUrlView](#loginurlview)
    - [MapFilter](#mapfilter)
        - [MapFilter().filter_member](#mapfilterfilter_member)
        - [MapFilter().filter_url](#mapfilterfilter_url)
    - [MapView](#mapview)
    - [PlayerPermissionMixin](#playerpermissionmixin)
        - [PlayerPermissionMixin().is_banned](#playerpermissionmixinis_banned)
    - [ReportView](#reportview)
    - [RoomAccessFilter](#roomaccessfilter)
        - [RoomAccessFilter().filter_player](#roomaccessfilterfilter_player)
        - [RoomAccessFilter().filter_url](#roomaccessfilterfilter_url)
    - [RoomAccessView](#roomaccessview)
    - [RoomSameWorldFilter](#roomsameworldfilter)
        - [RoomSameWorldFilter().filter_rooms](#roomsameworldfilterfilter_rooms)
    - [RoomSameWorldView](#roomsameworldview)
    - [getUserId](#getuserid)

## BanFilter

[[find in source code]](blob/master/wa_api/views.py#L39)

```python
class BanFilter(django_filters.FilterSet):
```

Filter for bans
(just used to fix schema generation)

## CheckBanView

[[find in source code]](blob/master/wa_api/views.py#L190)

```python
class CheckBanView(generics.RetrieveAPIView):
```

### CheckBanView().get

[[find in source code]](blob/master/wa_api/views.py#L223)

```python
def get(request, *args, **kwargs):
```

Find out if user is banned and why.

### CheckBanView().get_object

[[find in source code]](blob/master/wa_api/views.py#L196)

```python
def get_object(request):
```

logic to retrieve bans:
if a roomUrl is provided a ban for the room is queried as well as a ban for the world in general
if the user is banned for the world it is also counted as a ban for every single room

## CrudObjectAccessViolationMixin

[[find in source code]](blob/master/wa_api/views.py#L164)

```python
class CrudObjectAccessViolationMixin():
```

DRF expects object access via primary key. WA uses query_params without expecting a list.
Use this class for all WA get views unless overriding get method with different implementation.

### CrudObjectAccessViolationMixin().get

[[find in source code]](blob/master/wa_api/views.py#L170)

```python
def get(request, *args, **kwargs):
```

custom get method
use django filter package to access the object in question

## LoginUrlView

[[find in source code]](blob/master/wa_api/views.py#L252)

```python
class LoginUrlView(generics.RetrieveAPIView):
```

## MapFilter

[[find in source code]](blob/master/wa_api/views.py#L66)

```python
class MapFilter(PlayerPermissionMixin, django_filters.FilterSet):
```

Filter for Map

#### See also

- [PlayerPermissionMixin](#playerpermissionmixin)

### MapFilter().filter_member

[[find in source code]](blob/master/wa_api/views.py#L81)

```python
def filter_member(queryset, name, value):
```

### MapFilter().filter_url

[[find in source code]](blob/master/wa_api/views.py#L74)

```python
def filter_url(queryset, name, value):
```

We have to cut the domain from the request
Alternatively the Domain could get removed on login-url endpoint serializer

## MapView

[[find in source code]](blob/master/wa_api/views.py#L234)

```python
class MapView(CrudObjectAccessViolationMixin, generics.RetrieveAPIView):
```

#### See also

- [CrudObjectAccessViolationMixin](#crudobjectaccessviolationmixin)

## PlayerPermissionMixin

[[find in source code]](blob/master/wa_api/views.py#L53)

```python
class PlayerPermissionMixin():
```

mixin to check for bans

### PlayerPermissionMixin().is_banned

[[find in source code]](blob/master/wa_api/views.py#L58)

```python
def is_banned(membership, room_url):
```

## ReportView

[[find in source code]](blob/master/wa_api/views.py#L241)

```python
class ReportView(generics.CreateAPIView):
```

## RoomAccessFilter

[[find in source code]](blob/master/wa_api/views.py#L106)

```python
class RoomAccessFilter(PlayerPermissionMixin, django_filters.FilterSet):
```

Filter for RoomAccess

#### See also

- [PlayerPermissionMixin](#playerpermissionmixin)

### RoomAccessFilter().filter_player

[[find in source code]](blob/master/wa_api/views.py#L121)

```python
def filter_player(queryset, name, value):
```

1. if this is an email -> filter by email
2. if this is an uuid -> filter by uuid

### RoomAccessFilter().filter_url

[[find in source code]](blob/master/wa_api/views.py#L114)

```python
def filter_url(queryset, name, value):
```

We have to cut the domain from the request
Alternatively the Domain could get removed on login-url endpoint serializer

## RoomAccessView

[[find in source code]](blob/master/wa_api/views.py#L245)

```python
class RoomAccessView(
    CrudObjectAccessViolationMixin,
    generics.RetrieveAPIView,
):
```

#### See also

- [CrudObjectAccessViolationMixin](#crudobjectaccessviolationmixin)

## RoomSameWorldFilter

[[find in source code]](blob/master/wa_api/views.py#L144)

```python
class RoomSameWorldFilter(django_filters.FilterSet):
```

Filter for Rooms at same world endpoint

### RoomSameWorldFilter().filter_rooms

[[find in source code]](blob/master/wa_api/views.py#L151)

```python
def filter_rooms(queryset, name, value):
```

## RoomSameWorldView

[[find in source code]](blob/master/wa_api/views.py#L258)

```python
class RoomSameWorldView(generics.ListAPIView):
```

## getUserId

[[find in source code]](blob/master/wa_api/views.py#L31)

```python
def getUserId(self):
```
