# Asgi

> Auto-generated documentation for [wa_backstage.asgi](blob/master/wa_backstage/asgi.py) module.

ASGI config for wa_backstage project.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Wa Backstage](index.md#wa-backstage) / Asgi

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
