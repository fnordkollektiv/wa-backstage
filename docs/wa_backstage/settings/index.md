# Settings

> Auto-generated documentation for [wa_backstage.settings](blob/master/wa_backstage/settings/__init__.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Wa Backstage](../index.md#wa-backstage) / Settings
    - Modules
        - [Common](common.md#common)
        - [Local](local.md#local)
        - [Production](production.md#production)
