# Wsgi

> Auto-generated documentation for [wa_backstage.wsgi](blob/master/wa_backstage/wsgi.py) module.

WSGI config for wa_backstage project.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Wa Backstage](index.md#wa-backstage) / Wsgi

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/wsgi/
