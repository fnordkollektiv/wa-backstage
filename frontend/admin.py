from django.core.exceptions import ValidationError
from django.db.models.fields.files import FieldFile
from import_export import resources
from import_export.fields import Field
from tablib import import_set

from core.models import Player


def import_players(field_file: FieldFile, world=None, dry_run=False):
    field_file.open()
    data_str = field_file.file.read().decode()
    dataset = import_set(data_str, format="csv")
    if len(dataset) == 0:
        raise ValidationError("Empty dataset", code="empty")
    return PlayerResource(world).import_data(dataset, dry_run=dry_run)


class PlayerResource(resources.ModelResource):
    access_url = Field(readonly=True)

    class Meta:
        model = Player
        fields = ["access_url", "email", "username"]
        export_order = ["username", "email", "access_url"]
        skip_diff = True
        clean_model_instances = True
        force_init_instance = True

    def __init__(self, world=None):
        super().__init__()
        self.world = world

    def after_save_instance(self, instance, using_transactions, dry_run):
        if not dry_run:
            instance.world_memberships.get_or_create(world=self.world)

    def dehydrate_access_url(self, player):
        return player.world_memberships.get(world=self.world).get_world_access_url()

    def get_queryset(self):
        return super().get_queryset().filter(worlds=self.world)
