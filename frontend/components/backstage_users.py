from django.urls import reverse_lazy

from core.models import BackstageUser
from frontend.forms import BackstageUserForm
from unicorn_viewsets.mixins import CreateModeMixin, DeleteModeMixin, UpdateModeMixin
from unicorn_viewsets.views import GenericModeView


class BackstageUsersView(CreateModeMixin, DeleteModeMixin, UpdateModeMixin, GenericModeView):
    model = BackstageUser
    form_class = BackstageUserForm
    success_url = reverse_lazy("backstage-users")

    email = ""
    username = ""

    def get_form_data(self):
        return {
            "email": self.email,
            "username": self.username,
        }

    def set_attributes(self):
        super().set_attributes()
        self.email = self.object.email
        self.username = self.object.username
