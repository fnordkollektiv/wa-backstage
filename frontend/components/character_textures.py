from django.utils.translation import gettext as _
from django.urls import reverse_lazy
from core.models import CharacterTexture, World, PolicyType, Tag
from frontend.forms import CharacterTextureForm
from unicorn_viewsets.mixins import CreateModeMixin, DeleteModeMixin, UpdateModeMixin
from unicorn_viewsets.views import GenericModeView
from frontend.utils import FilterSelectWidgetDecorator


@FilterSelectWidgetDecorator(queryset=Tag.objects.exclude(name="admin"), form_field="tags")
@FilterSelectWidgetDecorator(queryset=World.objects.all(), form_field="worlds")
class CharacterTexturesView(CreateModeMixin, DeleteModeMixin, UpdateModeMixin, GenericModeView):
    model = CharacterTexture
    form_class = CharacterTextureForm
    success_url = reverse_lazy("character-textures")

    # lazy loading whatever bullshit with choices class
    policy_type_choices = [(choice[0], str(choice[1])) for choice in PolicyType.choices]
    # unicorn somewhere converts numbers to string on the way. So all strings...
    filter_choices = [["0", _("all")]] + [[str(world.pk), world.name] for world in World.objects.all()]
    selected_filter_choice = "0"

    name = ""
    url = ""
    texture_id = 0
    level = 0
    policy_type = 0
    is_deactivated = False

    world = None

    def filtered_list(self):
        # are we in world context?
        if self.world:
            return self.world.character_textures.all()
        if self.selected_filter_choice == "0":
            return CharacterTexture.objects.all()
        else:
            return CharacterTexture.objects.filter(world__in=[World.objects.get(pk=int(self.selected_filter_choice))])

    def get_form_data(self):
        return {
            "name": self.name,
            "url": self.url,
            "texture_id": self.texture_id,
            "level": self.level,
            "policy_type": self.policy_type,
            "is_deactivated": self.is_deactivated,
        }

    def set_attributes(self):
        super().set_attributes()
        self.name = self.object.name
        self.url = self.object.url
        self.texture_id = self.object.texture_id
        self.level = self.object.level
        self.policy_type = self.object.policy_type
        self.is_deactivated = self.object.is_deactivated

    def get_mode_kwargs(self):
        if self.world:
            return {"world_pk": self.world.pk}
        else:
            return super().get_mode_kwargs()

    def prepopulate_fields(self, form):
        if self.world:
            form.data["worlds"] = [self.world]

    class Meta(GenericModeView.Meta):
        javascript_exclude = ["object"]
