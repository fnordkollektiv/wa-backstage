from django.urls import reverse_lazy

from core.models import Player
from frontend.forms import PlayerForm
from frontend.views import ReferralMixin
from unicorn_viewsets.mixins import CreateModeMixin, DeleteModeMixin, UpdateModeMixin
from unicorn_viewsets.views import GenericModeView


class PlayersView(ReferralMixin, CreateModeMixin, DeleteModeMixin, UpdateModeMixin, GenericModeView):
    model = Player
    form_class = PlayerForm
    success_url = reverse_lazy("players")

    comment = ""
    email = ""
    username = ""

    def get_form_data(self):
        return {
            "comment": self.comment,
            "email": self.email,
            "username": self.username,
        }

    def set_attributes(self):
        super().set_attributes()
        self.comment = self.object.comment
        self.email = self.object.email
        self.username = self.object.username
