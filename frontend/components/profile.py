from django.conf import settings
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.shortcuts import redirect
from django.utils.translation import gettext as _
from django_unicorn.components import UnicornView

from frontend.forms import ProfileForm


class ProfileView(UnicornView):
    form_class = ProfileForm

    username = ""
    email = ""
    old_password = ""
    new_password1 = ""
    new_password2 = ""

    def redirect_to_success_url(self):
        return redirect(self.get_success_url())

    def validate_and_update(self):
        if self.is_valid():
            form = self._get_form(self._attributes())
            form.save()
            update_session_auth_hash(self.request, form.instance)
            self.old_password = self.new_password1 = self.new_password2 = ""
            messages.success(self.request, _("Profile updated"))

    def get_context_data(self, **kwargs):
        self.username = self.request.user.username
        self.email = self.request.user.email
        return super().get_context_data(**kwargs)

    def get_form_kwargs(self):
        return {"instance": self.request.user}

    def get_success_url(self):
        return settings.LOGIN_REDIRECT_URL

    def _get_form(self, data):
        form_kwargs = self.get_form_kwargs()
        form_kwargs["data"] = data
        form = self.form_class(**form_kwargs)
        form.is_valid()
        return form
