from django.urls import reverse
from django.db.models import Max
from core.models import CharacterTexture, Room, Tag, World, PolicyType
from frontend.forms import RoomForm
from unicorn_viewsets.mixins import CreateModeMixin, DeleteModeMixin, UpdateModeMixin
from unicorn_viewsets.views import GenericModeView
from frontend.utils import FilterSelectWidgetDecorator


@FilterSelectWidgetDecorator(queryset=Tag.objects.exclude(name="admin"), form_field="tags")
class RoomsView(CreateModeMixin, DeleteModeMixin, UpdateModeMixin, GenericModeView):
    model = Room
    form_class = RoomForm

    world = None

    # lazy loading whatever bullshit with choices class
    policy_type_choices = [(choice[0], str(choice[1])) for choice in PolicyType.choices]

    all_worlds = None
    all_character_textures = None

    name = ""
    description = ""
    url = ""
    map_url = ""
    is_entry_room = False

    policy_type = Room.policy_type.field.default
    is_deactivated = Room.is_deactivated.field.default

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._tmp_character_textures = []

        self.all_worlds = World.objects.all()
        # The pk is not yet known when creating a room. Therefore, the pk of the new room existing after creation
        # must be calculated.
        self.future_pk = Room.objects.all().aggregate(Max("pk"))["pk__max"] + 1
        self.all_character_textures = CharacterTexture.objects.all()

    def get_form_data(self):
        data = super().get_form_data()
        data.update(
            {
                "name": self.name,
                "description": self.description,
                "url": self.url,
                "map_url": self.map_url,
                "is_entry_room": self.is_entry_room,
                "policy_type": self.policy_type,
                "is_deactivated": self.is_deactivated,
            }
        )
        return data

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["instance"].world = self.world
        return kwargs

    def get_mode_kwargs(self):
        return {"world_pk": self.world.pk}

    def get_queryset(self):
        return self.world.rooms.order_by("-is_entry_room", "name")

    def get_success_url(self):
        return reverse("rooms", kwargs=self.get_mode_kwargs())

    def set_attributes(self):
        super().set_attributes()

        self.name = self.object.name
        self.description = self.object.description
        self.url = self.object.url
        self.map_url = self.object.map_url
        self.is_entry_room = self.object.is_entry_room
        self.policy_type = self.object.policy_type
        self.is_deactivated = self.object.is_deactivated
