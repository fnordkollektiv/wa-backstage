from django.urls import reverse_lazy

from core.models import Tag
from frontend.forms import TagForm
from unicorn_viewsets.mixins import CreateModeMixin, DeleteModeMixin, UpdateModeMixin
from unicorn_viewsets.views import GenericModeView


class TagsView(CreateModeMixin, DeleteModeMixin, UpdateModeMixin, GenericModeView):
    model = Tag
    form_class = TagForm
    success_url = reverse_lazy("tags")

    name = ""
    description = ""

    def get_form_data(self):
        return {
            "name": self.name,
            "description": self.description,
        }

    def set_attributes(self):
        super().set_attributes()
        self.name = self.object.name
        self.description = self.object.description
