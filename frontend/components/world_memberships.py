from django.urls import reverse, reverse_lazy

from core.models import MembershipWorld, Player, Tag, AdminTag
from frontend.forms import MembershipWorldForm
from unicorn_viewsets.mixins import DeleteModeMixin, UpdateModeMixin
from unicorn_viewsets.views import GenericModeView
from frontend.utils import FilterSelectWidgetDecorator


@FilterSelectWidgetDecorator(queryset=Tag.objects.exclude(name="admin"), form_field="tags")
class WorldMembershipsView(UpdateModeMixin, DeleteModeMixin, GenericModeView):
    model = MembershipWorld
    form_class = MembershipWorldForm
    success_url = reverse_lazy("worlds")

    addable_players = []
    is_create_hidden = True
    world = None

    is_admin = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._tmp_tags = []

    def toggle_create(self):
        self.is_create_hidden = not self.is_create_hidden
        self.set_attributes()

    def create_membership(self, player_pk):
        player = Player.objects.get(pk=player_pk)
        self.world.memberships.create(player=player)
        self.set_attributes()

    def get_mode_kwargs(self):
        return {"world_pk": self.world.pk}

    def get_queryset(self):
        return self.world.memberships.exclude_anonymous()

    def get_success_url(self):
        return reverse("world-memberships", kwargs=self.get_mode_kwargs())

    def get_form_data(self):
        """At this point this looks like an empty return. However, this method will be modified by the decorators."""
        return {"is_admin": self.is_admin}

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # kwargs["data"]["tags"] = Tag.objects.filter(pk__in=[tag["pk"] for tag in self.tags])
        kwargs["instance"].world = self.world
        return kwargs

    def set_attributes(self):
        super().set_attributes()
        if not self.is_create_hidden:
            self.addable_players = Player.objects.exclude(worlds=self.world)
        if self.mode == "update":
            self.is_admin = self.object.is_admin()

    # override to honor is_admin unbound field
    def validate_and_save_form(self):
        # we cannot call super because it triggers mode change already
        form = self.get_form()
        print(form.data)
        is_valid = form.is_valid()
        self.errors = self.get_errors(form)
        if is_valid:
            form.save()
            # add or remove admin tag
            if form.data["is_admin"]:
                form.instance.tags.add(AdminTag.objects.first())
            else:
                form.instance.tags.remove(AdminTag.objects.first())
            form.instance.save()
            return self.switch_mode()
