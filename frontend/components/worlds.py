from django.urls import reverse_lazy

from core.models import World
from frontend.forms import WorldForm
from unicorn_viewsets.mixins import CreateModeMixin, DeleteModeMixin, UpdateModeMixin
from unicorn_viewsets.views import GenericModeView


class WorldsView(CreateModeMixin, UpdateModeMixin, DeleteModeMixin, GenericModeView):
    model = World
    form_class = WorldForm
    success_url = reverse_lazy("worlds")

    name = ""
    description = ""
    url = ""

    def set_attributes(self):
        super().set_attributes()
        self.name = self.object.name
        self.description = self.object.description
        self.url = self.object.url

    def get_form_data(self):
        return {"name": self.name, "description": self.description, "url": self.url}
