from django import forms
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.utils.translation import gettext as _
from tablib import InvalidDatasetType, InvalidDimensions, UnsupportedFormat
from core.models import CharacterTexture, World, Ban
from django.core.validators import RegexValidator
from django.db import models
from frontend.admin import import_players
from core.models import BackstageUser, Player, Room, Tag, MembershipWorld


class BackstageUserForm(forms.ModelForm):
    class Meta:
        model = BackstageUser
        fields = ["username", "email"]

    def clean_email(self):
        return BackstageUser.objects.normalize_email(self.cleaned_data["email"])

    def clean_username(self):
        return BackstageUser.normalize_username(self.cleaned_data["username"])


class WorldForm(forms.ModelForm):
    class Meta:
        model = World
        fields = ["name", "description", "url"]


class PlayerForm(forms.ModelForm):
    class Meta:
        model = Player
        fields = ["comment", "email", "username"]


class ImportForm(forms.Form):
    file = forms.FileField()

    def clean_file(self):
        uploaded_file = self.cleaned_data["file"]
        try:
            result = import_players(uploaded_file, dry_run=True)
        except (InvalidDatasetType, InvalidDimensions, UnsupportedFormat, ValidationError) as e:
            raise ValidationError(_("Not a valid CSV file"), code="invalid-format") from e
        if result.has_errors() or result.has_validation_errors():
            raise ValidationError(_("Error in data"), code="invalid-data")
        return uploaded_file

    def clean_email(self):
        return Player.objects.normalize_email(self.cleaned_data["email"])

    def clean_username(self):
        return Player.normalize_username(self.cleaned_data["username"])


class RoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = [
            "name",
            "description",
            "url",
            "is_entry_room",
            "map_url",
            "tags",
            "policy_type",
            "is_deactivated",
        ]

        map_url = models.URLField(
            max_length=1024,
            verbose_name=_("Map URL"),
            blank=True,
            error_messages={"invalid": "You need to enter a valid URL that also points to a JSON file."},
            validators=[RegexValidator(r"[\s\S]*\.json$", message=_("Map URL needs to point to a json file."))],
        )


class MembershipWorldForm(forms.ModelForm):
    is_admin = forms.BooleanField(required=False)

    class Meta:
        model = MembershipWorld
        fields = ["tags", "is_admin"]


class BanForm(forms.ModelForm):
    class Meta:
        model = Ban
        fields = ["membership_world", "room", "message"]

    def save(self, commit=True):
        try:
            return super().save(commit)
        except IntegrityError as e:
            raise ValidationError("A ban for this player for this whole world already exists.") from e


class ProfileForm(forms.ModelForm):
    old_password = forms.CharField(required=False)
    new_password1 = forms.CharField(required=False)
    new_password2 = forms.CharField(required=False)

    class Meta:
        model = BackstageUser
        fields = ["username", "email"]

    def clean_old_password(self):
        old_password = self.cleaned_data["old_password"]
        if old_password and not self.instance.check_password(old_password):
            raise ValidationError(_("Wrong password"))
        return old_password

    def clean_new_password1(self):
        password = self.cleaned_data["new_password1"]
        if password:
            validate_password(password, self.instance)
        return password

    def clean(self):
        cleaned_data = super().clean()
        old_password = cleaned_data.get("old_password")
        new_password1 = cleaned_data.get("new_password1")
        new_password2 = cleaned_data.get("new_password2")
        if new_password1 and not old_password:
            raise ValidationError(_("Please provide current password"))
        if (new_password1 or new_password2) and not (new_password1 == new_password2):
            raise ValidationError(_("Passwords do not match"))
        return cleaned_data

    def save(self, commit=True):
        password = self.cleaned_data["new_password1"]
        if password:
            self.instance.set_password(password)
        return super().save(commit)


class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ["name", "description"]


class CharacterTextureForm(forms.ModelForm):
    class Meta:
        model = CharacterTexture
        fields = ["name", "url", "texture_id", "level", "policy_type", "tags", "worlds", "is_deactivated"]
