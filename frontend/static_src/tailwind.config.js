/**
 * This is a minimal config.
 *
 * If you need the full config, get it from here:
 * https://unpkg.com/browse/tailwindcss@latest/stubs/defaultConfig.stub.js
 */

module.exports = {
    content: [
        /**
         * HTML. Paths to Django template files that will contain Tailwind CSS classes.
         */

        /*  Templates within theme app (<tailwind_app_name>/templates), e.g. base.html. */
        '../templates/**/*.html',

        /*
         * Main templates directory of the project (BASE_DIR/templates).
         * Adjust the following line to match your project structure.
         */
        '../../templates/**/*.html',

        /*
         * Templates in other django apps (BASE_DIR/<any_app_name>/templates).
         * Adjust the following line to match your project structure.
         */
        '../../**/templates/**/*.html',

        /**
         * JS: If you use Tailwind CSS in JavaScript, uncomment the following lines and make sure
         * patterns match your project structure.
         */
        /* JS 1: Ignore any JavaScript in node_modules folder. */
        // '!../../**/node_modules',
        /* JS 2: Process all JavaScript files in the project. */
        // '../../**/*.js',

        /**
         * Python: If you use Tailwind CSS classes in Python, uncomment the following line
         * and make sure the pattern below matches your project structure.
         */
        // '../../**/*.py'
    ],
    theme: {
        extend: {
            fontFamily:{
                bungee: ['Bungee', 'sans-serif'],
                blinker: ['Blinker', 'sans-serif']
            },
            inset: {
                '27': '6.75rem'
            },
        },
        colors: {
            green: {
                DEFAULT: '#5BAF28',
                dark: '#0d791F'
             } ,
            purple: {
                DEFAULT: '#660080'
            } ,
            orange: {
                DEFAULT: '#FA6400'
            } ,
            red: {
                light: '#F35C5C',
                DEFAULT: '#E31C1CFF'
            },
            blue: {
                DEFAULT: '#0055D4'
            },
            grey: {
                1: '#F9F9F9',
                2: '#ECECEC',
                3: '#CCCCCC',
                4: '#757575',
                5: '#2B222D'
            },
            white: {
                DEFAULT: '#FFFFFF'
            },
            black: {
                DEFAULT: '#000000'
            },
        },
    },
    plugins: [
        /**
         * '@tailwindcss/forms' is the forms plugin that provides a minimal styling
         * for forms. If you don't like it or have own styling for forms,
         * comment the line below to disable '@tailwindcss/forms'.
         */
        require('@tailwindcss/forms'),
        require('@tailwindcss/typography'),
        require('@tailwindcss/line-clamp'),
        require('@tailwindcss/aspect-ratio'),
    ],
}
