import urllib.parse
import re
from django import template


from frontend.views import ReferralMixin


register = template.Library()


@register.filter
def aselementid(value, arg):
    return f"{arg}-{value}"


@register.filter
def startswith(value, arg):
    return value.startswith(arg)


@register.filter
def withparam(value, arg):
    return f"{value}({arg})"


@register.filter
def withreferrer(value, arg):
    return f"{value}?{ReferralMixin.REFERRER_PARAMETER_NAME}={urllib.parse.quote(arg)}"


@register.filter
def withstringparam(value, arg):
    return withparam(value, f"'{arg}'")


@register.filter
def concaturlwith(value, arg):
    return value + arg + "/"


@register.filter
def openforall(value):
    return value == 1


@register.filter
def joinnoadmin(value):
    return ", ".join([v.name for v in value if v.name != "admin"])


@register.filter
def prepend_url(value, arg):
    return str(arg) + str(value)


@register.filter
def ifnoroomall(value):
    return "all rooms" if not value else value


@register.filter
def isopenforall(value):
    return (value.policy_type == 1) if value is not None else False


@register.filter
def replaceextension(value, arg):
    return re.sub(r"[a-zA-Z0-9]+\.json$", "map.png", value)


@register.inclusion_tag("frontend/partials/filterselect_widget.html", takes_context=True)
def filterselect_widget(context, form_field, label, name_plural):
    prefix = form_field + "__"
    return {
        "input_label": label,
        "input_id": form_field,
        "unicorn_model": prefix + "suggested_instances_name_filter",
        "get_suggested_instances": context[prefix + "get_suggested_instances"],
        "selected_instances": "selected_" + form_field,
        "add_instance": prefix + "add_instance",
        "name_plural": name_plural,
        "added_instances": context[prefix + "added_instances"],
        "remove_instance": prefix + "remove_instance",
    }
