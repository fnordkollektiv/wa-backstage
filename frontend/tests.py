from django.test import TestCase
from django.urls import reverse

from core.models import BackstageUser


class AuthenticatedFrontendTestCase(TestCase):
    def setUp(self):
        user = BackstageUser.objects.create_user("test")
        self.client.force_login(user)

    def test_players_page_renders_successfully(self):
        """Player list should render without errors."""
        url = reverse("players")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_users_page_renders_successfully(self):
        """User list should render without errors.

        Regression test for #118.
        """
        url = reverse("backstage-users")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_worlds_page_renders_successfully(self):
        """World list should render without errors."""
        url = reverse("worlds")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
