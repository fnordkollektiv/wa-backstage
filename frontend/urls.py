from django.conf import settings
from django.urls import include, path
from django.views.generic import RedirectView, TemplateView

from frontend.views import AboutView, ComponentView, PlayerExportView, PlayerImportView, WorldComponentView
from unicorn_viewsets.routers import Router

router = Router(view_class=ComponentView)
router.register("backstage-users", "backstage-users")
router.register("players", "players")
router.register("worlds", "worlds")
router.register("tags", "tags")
router.register("character-textures", "character-textures")

world_router = Router(view_class=WorldComponentView)
world_router.register("rooms", "rooms")
world_router.register("players", "world-memberships")
world_router.register("character-textures", "character-textures")
world_router.register("bans", "bans")

urlpatterns = [
    path("", include(router.urls)),
    path("", RedirectView.as_view(url=settings.LOGIN_REDIRECT_URL), name="index"),
    path("about/", AboutView.as_view(), name="about"),
    path("accounts/", include("django.contrib.auth.urls")),
    path("css-components/", TemplateView.as_view(template_name="css-components/css-components.html")),
    path("profile/edit/", ComponentView.as_view(component_name="profile"), name="profile-update"),
    path("worlds/<int:world_pk>/", include(world_router.urls)),
    path(
        "worlds/<int:world_pk>/bans/add/('<uuid:membership_pk>')/",
        ComponentView.as_view(component_name="bans"),
        name="bans-create",
    ),
    path("worlds/<int:world_pk>/players/export/", PlayerExportView.as_view(), name="export-world-players"),
    path("worlds/<int:world_pk>/players/import/", PlayerImportView.as_view(), name="import-world-players"),
]
