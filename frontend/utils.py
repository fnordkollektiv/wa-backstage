from unicorn_viewsets.views import GenericModeView


class FilterSelectWidgetDecorator:
    """This is a decorator to add autocomplete widgets to unicorn components
    use as a class decorator to the component:
    @FilterSelectWidgetDecorator(queryset=<qs>, form_field="field_name")
    Managed items must have pk and name fields
    """

    @staticmethod
    def main__set_attributes(self):
        self.set_attributes_undecorated()
        for entry in self.filterselect_widgets:
            getattr(self, entry + "__set_attributes")()

    @staticmethod
    def main__get_form_data(self):
        data = self.get_form_data_undecorated()
        for entry in self.filterselect_widgets:
            data = getattr(self, entry + "__get_form_data")(data)
        return data

    def __init__(self, *decorator_args, **decorator_kwargs):
        try:
            self.queryset = decorator_kwargs["queryset"]
            self.form_field = decorator_kwargs["form_field"]
        except KeyError:
            raise KeyError("queryset and form_field arguments need to be specified on the decorator")

    def __call__(self, decorated_class):  # noqa c901
        # extra attributes needed for autocomplete
        extra_attrs = {}
        extra_attrs["queryset"] = self.queryset
        extra_attrs["added_instances"] = []
        extra_attrs["suggested_instances_name_filter"] = ""

        # extra methods we needed for autocomplete
        extra_methods = []

        def add_instance(self, pk, prefix=self.form_field + "__"):
            instance = getattr(self, prefix + "queryset").get(pk=pk)
            getattr(self, prefix + "added_instances").append(instance)

        extra_methods.append(add_instance)

        def get_form_data(self, data, prefix=self.form_field + "__", form_field=self.form_field):
            data[form_field] = getattr(self, prefix + "added_instances")
            return data

        extra_methods.append(get_form_data)

        def get_object_instances(self, prefix=self.form_field + "__", form_field=self.form_field):
            if self.object.pk:
                return getattr(self.object, form_field).all()
            else:
                return getattr(self, prefix + "queryset").none()

        extra_methods.append(get_object_instances)

        def get_suggested_instances(self, prefix=self.form_field + "__"):
            if getattr(self, prefix + "suggested_instances_name_filter"):
                added_instance_pks = map(lambda i: i.pk, getattr(self, prefix + "added_instances"))
                instances = (
                    getattr(self, prefix + "queryset")
                    .filter(name__icontains=getattr(self, prefix + "suggested_instances_name_filter"))
                    .exclude(pk__in=added_instance_pks)
                )
            else:
                instances = getattr(self, prefix + "queryset").none()
            return instances

        extra_methods.append(get_suggested_instances)

        def remove_instance(self, pk, prefix=self.form_field + "__"):
            instance = getattr(self, prefix + "queryset").get(pk=pk)
            getattr(self, prefix + "added_instances").remove(instance)

        extra_methods.append(remove_instance)

        def set_attributes(self, prefix=self.form_field + "__"):
            setattr(self, prefix + "added_instances", list(set(getattr(self, prefix + "get_object_instances")())))
            setattr(self, prefix + "suggested_instances_name_filter", "")

        extra_methods.append(set_attributes)
        # add attributes to class
        for key, value in extra_attrs.items():
            setattr(decorated_class, self.form_field + "__" + key, value)

        # register decorators for set_attribute, get_form_data override
        # we move methods to different name (_undecorated)
        if not hasattr(decorated_class, "set_attributes_undecorated"):
            decorated_class.set_attributes.__name__ = "set_attributes_undecorated"
            decorated_class.get_form_data.__name__ = "get_form_data_undecorated"
            setattr(decorated_class, "set_attributes_undecorated", decorated_class.set_attributes)
            setattr(decorated_class, "get_form_data_undecorated", decorated_class.get_form_data)

        if not hasattr(decorated_class, "filterselect_widgets"):
            filterselect_widgets = [
                self.form_field,
            ]
            setattr(decorated_class, "filterselect_widgets", filterselect_widgets)
            self.main__get_form_data.__name__ = "get_form_data"
            self.main__set_attributes.__name__ = "set_attributes"
            setattr(decorated_class, "get_form_data", self.main__get_form_data)
            setattr(decorated_class, "set_attributes", self.main__set_attributes)
        else:
            decorated_class.filterselect_widgets.append(self.form_field)

        # add methods to class
        for method in extra_methods:
            method.__name__ = self.form_field + "__" + method.__name__
            setattr(decorated_class, method.__name__, method)

        # add js excludes to meta class to avoid the type casting pains
        # inherited Meta?
        if decorated_class.Meta.__module__ == "unicorn_viewsets.views":

            class Meta(GenericModeView.Meta):
                javascript_exclude = ["object", self.form_field + "__added_instances"]

            setattr(decorated_class, "Meta", Meta)
        # overriden Meta?
        else:
            if hasattr(decorated_class.Meta, "javascript_exclude"):
                decorated_class.Meta.javascript_exclude.append(self.form_field + "__added_instances")
            else:
                setattr(decorated_class.Meta, "javascript_exclude", [self.form_field + "__added_instances"])
        return decorated_class
