import urllib.parse

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import FormView, TemplateView
from django.views.generic.detail import SingleObjectMixin

from core.models import World
from frontend.admin import import_players, PlayerResource
from frontend.forms import ImportForm
from unicorn_viewsets import views
from wa_backstage import __version__


class ReferralMixin:
    REFERRER_PARAMETER_NAME = "next"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.referrer = self.request.GET.get(self.REFERRER_PARAMETER_NAME, "")

    def needs_page_reload(self):
        return bool(self.referrer)

    def get_success_url(self):
        return urllib.parse.unquote(self.referrer) or super().get_success_url()


class AboutView(TemplateView):
    template_name = "frontend/about.html"

    def get_context_data(self, **kwargs):
        kwargs["version"] = __version__
        return super().get_context_data(**kwargs)


class ComponentView(views.ComponentView):
    template_name = "frontend/component.html"


class WorldComponentView(SingleObjectMixin, views.ComponentView):
    model = World
    object = None
    pk_url_kwarg = "world_pk"
    template_name = "frontend/component_world.html"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class CSVResponse(HttpResponse):
    def __init__(self, content, filename, *args, **kwargs):
        super().__init__(content, content_type="text/csv")
        self["Content-Disposition"] = 'attachment; filename="{}"'.format(filename)


class PlayerExportView(View):
    def get(self, request, *args, **kwargs):
        world = get_object_or_404(World, pk=kwargs.get("world_pk"))
        dataset = PlayerResource(world).export()
        return CSVResponse(dataset.csv, "Players.csv")


class PlayerImportView(FormView):
    template_name = "frontend/import.html"
    form_class = ImportForm
    success_url = reverse_lazy("worlds")

    def form_valid(self, form):
        world = get_object_or_404(World, pk=self.kwargs.get("world_pk"))
        import_players(form.cleaned_data["file"], world)
        return super().form_valid(form)
