import re
from django.conf import settings
from django.http import HttpResponse


class PreSharedKeyAuthMiddleware:
    """This middleware checks if a pre shared secret is send as Authorization-Header
    It is applied to all paths under the /api/ endpoint
    Default: if not defined in patterns this middleware is not applied
    URL_ALLOW_PATTERNS override disallow patterns
    """

    URL_DISALLOW_PATTERNS = [r"^\/api\/.*"]
    URL_ALLOW_PATTERNS = [r"^\/api\/openapi$"]

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        # only apply this on wa_api
        # request here need to authenticate with the preshared secret

        # we expect a token to be defined as django setting
        # if not we deny auth
        if not hasattr(settings, "ADMIN_API_TOKEN"):
            return HttpResponse("Unauthorized", status=401)
        # let's see if our patterns matches
        matches = False
        for pattern in self.URL_DISALLOW_PATTERNS:
            if re.match(pattern, request.path):
                matches = True
        for pattern in self.URL_ALLOW_PATTERNS:
            if re.match(pattern, request.path):
                matches = False
        if matches:
            if "Authorization" in request.headers and request.headers["Authorization"] == settings.ADMIN_API_TOKEN:
                pass
            else:
                return HttpResponse("Unauthorized", status=401)

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    def process_view(self, request, view_func, view_args, view_kwargs):

        return None
