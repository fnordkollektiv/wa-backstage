# python imports

# django imports
from django.urls import include, path
from rest_framework import routers
from rest_framework.schemas import get_schema_view

# project imports
from wa_api.views import CheckBanView, MapView, ReportView, RoomAccessView, LoginUrlView, RoomSameWorldView

# define DRF routes
router = routers.DefaultRouter()

# define django routes
urlpatterns = [
    path("", include(router.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path(
        "openapi",
        get_schema_view(
            title="WA admin API",
            description="The API used by connected WA instances.",
            version="1.0.0",
            urlconf="wa_api.urls",
        ),
        name="wa_api-openapi-schema",
    ),
    path("ban", CheckBanView.as_view()),
    path("map", MapView.as_view()),
    path("report", ReportView.as_view()),
    path("room/access", RoomAccessView.as_view()),
    path("login-url/<uuid:pk>/", LoginUrlView.as_view()),
    path("room/sameWorld", RoomSameWorldView.as_view()),
]
