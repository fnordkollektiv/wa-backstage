# python imports
import uuid
from urllib import parse as urlparse

# django imports
from django.utils.translation import gettext as _
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.exceptions import ParseError, NotFound
import django_filters
from django_filters import rest_framework as filters
from rest_framework.schemas.openapi import AutoSchema

# project imports
from core.models import Ban, MembershipWorld, Room
from wa_api.serializers import (
    BanSerializer,
    MapSerializer,
    ReportSerializer,
    RoomAccessSerializer,
    AdminApiDataSerializer,
    RoomSameWorldSerializer,
)


# hacks

# # adds a runtime property to Room class to make filter handling possible and
# # not get this into core app
def getUserId(self):
    return "something"


Room.userId = property(getUserId)


# filters
class BanFilter(django_filters.FilterSet):
    """
    Filter for bans
    (just used to fix schema generation)
    """

    token = django_filters.UUIDFilter(field_name="membership_world__uuid_token", required=True)
    roomUrl = django_filters.CharFilter(field_name="room__url")

    class Meta:
        model = Ban
        fields = ["token", "roomUrl"]


class PlayerPermissionMixin:
    """
    mixin to check for bans
    """

    def is_banned(self, membership, room_url):
        room_url = urlparse.urlparse(self.data.get("playUri", self.data.get("roomId"))).path
        bans = Ban.objects.filter(membership_world=membership, room__isnull=True) | Ban.objects.filter(
            membership_world=membership, room__url=room_url
        )
        return bans.exists()


class MapFilter(PlayerPermissionMixin, django_filters.FilterSet):
    """
    Filter for Map
    """

    playUri = django_filters.CharFilter(field_name="url", method="filter_url", required=True)
    userId = django_filters.CharFilter(field_name="userId", method="filter_member")

    def filter_url(self, queryset, name, value):
        """
        We have to cut the domain from the request
        Alternatively the Domain could get removed on login-url endpoint serializer
        """
        return queryset.filter(url=urlparse.urlparse(value).path)

    def filter_member(self, queryset, name, value):
        # how a map is player dependent is still unclear
        if not value:
            return queryset
        # if banned we do not return a map
        room_url = urlparse.urlparse(self.data["playUri"]).path
        # userId can be uuid or email
        try:
            uuid_value = uuid.UUID(value)
            membership = MembershipWorld.objects.filter(uuid_token=uuid_value)
        except ValueError:
            membership = MembershipWorld.objects.filter(player__email=value)
        if not membership.exists():
            # also anonymous users get the map
            return queryset
        if self.is_banned(membership.first(), room_url):
            return Room.objects.none()
        else:
            return queryset

    class Meta:
        model = Room
        fields = ["playUri", "userId"]


class RoomAccessFilter(PlayerPermissionMixin, django_filters.FilterSet):
    """
    Filter for RoomAccess
    """

    userIdentifier = django_filters.CharFilter(field_name="uuid_token", method="filter_player", required=True)
    roomId = django_filters.CharFilter(field_name="world__rooms__url", method="filter_url", required=True)

    def filter_url(self, queryset, name, value):
        """
        We have to cut the domain from the request
        Alternatively the Domain could get removed on login-url endpoint serializer
        """
        return queryset.filter(world__rooms__url=urlparse.urlparse(value).path)

    def filter_player(self, queryset, name, value):
        """
        1. if this is an email -> filter by email
        2. if this is an uuid -> filter by uuid
        """
        if not value:
            return queryset.filter(anonymous=True)
        try:
            uuid_value = uuid.UUID(value)
            filtered_queryset = queryset.filter(uuid_token=uuid_value)
        except ValueError:
            filtered_queryset = queryset.filter(player__email=value)
        room_url = urlparse.urlparse(self.data["roomId"]).path
        if filtered_queryset.exists() and self.is_banned(filtered_queryset.first(), room_url):
            return MembershipWorld.objects.none()
        else:
            return filtered_queryset

    class Meta:
        model = MembershipWorld
        fields = ["userIdentifier", "roomId"]


class RoomSameWorldFilter(django_filters.FilterSet):
    """
    Filter for Rooms at same world endpoint
    """

    roomUrl = django_filters.CharFilter(field_name="url", required=True, method="filter_rooms")

    def filter_rooms(self, queryset, name, value):
        try:
            same_world = Room.objects.get(url=urlparse.urlparse(value).path).world
        except ObjectDoesNotExist:
            raise NotFound
        return queryset.filter(world=same_world)

    class Meta:
        model = Room
        fields = ["roomUrl"]


# views
class CrudObjectAccessViolationMixin:
    """
    DRF expects object access via primary key. WA uses query_params without expecting a list.
    Use this class for all WA get views unless overriding get method with different implementation.
    """

    def get(self, request, *args, **kwargs):
        """
        custom get method
        use django filter package to access the object in question
        """
        # required validation not working -> fix
        filterset = self.filterset_class(request.query_params, queryset=self.get_queryset())
        for filter_name, filter_obj in filterset.get_filters().items():
            required = filter_obj.extra["required"]
            if required and (required != (filter_name in request.query_params.keys())):
                raise ParseError(detail=_(f"required param {filter_name} missing"))

        queryset = filterset.qs
        if queryset.exists():
            serializer = self.get_serializer(queryset.first())
            return Response(serializer.data)
        else:
            raise NotFound


class CheckBanView(generics.RetrieveAPIView):
    queryset = Ban.objects.all()
    serializer_class = BanSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = BanFilter

    def get_object(self, request):
        """
        logic to retrieve bans:
        if a roomUrl is provided a ban for the room is queried as well as a ban for the world in general
        if the user is banned for the world it is also counted as a ban for every single room
        """
        try:
            token = uuid.UUID(request.GET["token"])
        except KeyError:
            raise ParseError(detail=_("Token missing."))
        except ValueError:
            raise NotFound(detail=_("Not found."))
        room_url = request.GET.get("roomUrl", None)
        if room_url and Room.objects.filter(url=room_url).exists():
            queryset = Ban.objects.filter(membership_world__uuid_token=token, room__url=room_url) | Ban.objects.filter(
                membership_world__uuid_token=token
            )
        else:
            queryset = Ban.objects.filter(membership_world__uuid_token=token, room__url__isnull=True)
        if not queryset:
            not_banned_instance = Ban(
                message="",
            )
            return not_banned_instance
        else:
            return queryset.first()

    def get(self, request, *args, **kwargs):
        """
        Find out if user is banned and why.
        """
        instance = self.get_object(request)
        is_banned = True if instance.pk else False

        serializer = self.get_serializer(instance, is_banned=is_banned)
        return Response(serializer.data)


class MapView(CrudObjectAccessViolationMixin, generics.RetrieveAPIView):
    queryset = Room.objects.exclude(is_deactivated=True)
    serializer_class = MapSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = MapFilter


class ReportView(generics.CreateAPIView):
    serializer_class = ReportSerializer


class RoomAccessView(CrudObjectAccessViolationMixin, generics.RetrieveAPIView):
    queryset = MembershipWorld.objects.all()
    serializer_class = RoomAccessSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = RoomAccessFilter


class LoginUrlView(generics.RetrieveAPIView):
    schema = AutoSchema(operation_id_base="AdminApiData")
    serializer_class = AdminApiDataSerializer
    queryset = MembershipWorld.objects.all()


class RoomSameWorldView(generics.ListAPIView):
    queryset = Room.objects.exclude(is_deactivated=True)
    serializer_class = RoomSameWorldSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = RoomSameWorldFilter
